import { Component } from "react";
import { createClient } from "contentful";

class ContentfulService extends Component {
  constructor(props) {
    super(props);
    this.client = createClient({
      space: process.env.REACT_APP_SPACE_ID,
      accessToken: process.env.REACT_APP_ACCESS_TOKEN
    });
  }

  getEntry(id) {
    return this.client.getEntry(id);
  }

  getJobExperiences(type) {
    return this.client.getEntries({
      content_type: "experience",
      order: 'fields.startDate'
    });
  }
}

export default ContentfulService;
