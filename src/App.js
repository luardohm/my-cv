import React from "react";

import ContactMe from "./Pages/ContactMe";
import Expertise from "./Pages/Expertice";
import Intro from "./Pages/Intro";
import Skills from "./Pages/Skills";

import Section from "./Components/Section";
import Avatar from "./Components/Avatar";

import WebFont from "webfontloader";
import "./styles/styles.css";
import {Experience} from "./Pages/Experience/";
import {About} from "./Pages/About/";

WebFont.load({
  google: {
    families: ["Roboto"]
  }
});


const App = () => (
  <div className="main">
    <Section image={<Avatar />} modifierClass="main" body={<Intro />} />
    <Section
      title="Who I am"
      subtitle="All about me"
      body={<About pageId="4pUkIzwVHWOm2Qmk2k46YQ" />}
    />
    <Section
      title="Expertise"
      subtitle="This is what I am good at"
      body={<Expertise />}
    />
    <Section
      title="Experience"
      subtitle="This is what I am good at"
      body={<Experience />}
    />

    <Section
      title="Skills"
      subtitle="Let's get more specific"
      body={<Skills />}
    />
    <Section
      title="Get in touch"
      subtitle="Let's work together"
      body={<ContactMe />}
    />
  </div>
);

export default App;
