const size = {
  small: '640px',
  medium: '920px',
  large: '1024px',
  xlarge: '1600px'
};


export const viewports = {
  small: `(min-width: ${size.small})`,
  medium: `(min-width: ${size.medium})`,
  large: `(min-width: ${size.large})`,
  xlarge: `(min-width: ${size.xlarge})`
};
