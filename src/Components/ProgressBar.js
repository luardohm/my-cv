import React from "react";
import styled from 'styled-components';

const ProgressBarWrapper = styled.div`
  margin-bottom: 25px;
`;

const Header = styled.div`
  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  padding-bottom: 10px;
`;

const PercentValueInText = styled.div`
  margin-left: 0;
  margin-right: 0;
  width: 3rem;
  font-weight: bold;
  color: #F8BB10;
`;

const Label = styled.div`
  flex: 1;
  font-weight: bold;
  text-transform: uppercase;
  color: #727272;
  `;

const GraphicProgressBarWrapper = styled.div`
  background: #ececec;
  position: relative;
  height: 5px;
`;

const GraphicProgressBarPercent = styled.div`
  background: #F8BB10;
  transition: width 1.4s ease-in-out .5s;
  height: 100%;
  width: ${props => props.percentValue + '%' || 0}; 
`;

const ProgressBar = ({percent, title}) => (
    <ProgressBarWrapper>
      <Header>
        <PercentValueInText>{percent}%</PercentValueInText>
        <Label>{title}</Label>
      </Header>

      <GraphicProgressBarWrapper>
        <GraphicProgressBarPercent percentValue={percent} />
      </GraphicProgressBarWrapper>
    </ProgressBarWrapper>
);

export default ProgressBar;
