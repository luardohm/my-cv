import React from "react";
import styled from 'styled-components';
import Markdown from 'markdown-to-jsx';

const ParragraphContainer = styled.div`
    font-size: 16px;
    line-height: 1.5;
    color: #999999;
`;

const Paragraph = props => (
  <div
    className="paragraph"
    dangerouslySetInnerHTML={{ __html: props.content }}
  />
);

export default Paragraph;
