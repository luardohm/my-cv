import React from "react";
import styled from 'styled-components';
import {viewports} from "../utils/viewports";

const Wrapper = styled.div`
  text-align: center;
  padding-bottom: 20px;
  
  @media ${viewports.large} {
    text-align: left;
  }
`;

const ContactText = styled.p`
  font-size: 14px;
  font-weight: ${props => ( props.bold ? 'bold' :  'normal')}
`;

const ContactItem = ({label, text}) => (
    <Wrapper>
      <ContactText as="h4" bold>{label}</ContactText>
      <ContactText>{text}</ContactText>
    </Wrapper>
);

export default ContactItem;
