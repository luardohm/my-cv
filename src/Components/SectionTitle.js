import React from "react";
const SectionTitle = ({ title, subtitle }) => (
  <div className="section-title">
    <h2 className="section-title__text">
      {" "}
      {title} <span className="section-title__subtext"> {subtitle}</span>
    </h2>
  </div>
);

export default SectionTitle;
