import React from "react";
import styled from "styled-components";

const TitleWrapper = styled.div`
  text-align: center;
  
   @media only screen and (min-width: 1024px) {
     text-align: left;
    }
`;

const Name = styled.div`
    font-size: 2em;
    font-weight: 100;
    color: #999999;
`;

const LastName = styled.div`
    font-size: 2.5em;
    color: #F8BB10;
    font-weight: bold;
    text-transform: uppercase;
`;

const JobPosition = styled.div`
    padding-top: 10px;
    font-size: 1em;
    font-weight: 100;
    color: #999999;
`;

const Title = ({ name, lastname, position }) => (
  <TitleWrapper className="title">
    <Name>{name}</Name>
    <LastName>{lastname}</LastName>
    <JobPosition>{position}</JobPosition>
  </TitleWrapper>
);

export default Title;
