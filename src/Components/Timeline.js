import React from "react";
import styled from "styled-components";
import marked from "marked";

const TimeLineItem = styled.div`
  position: relative;
`;

const TimeLinePoint = styled.div`
  background: #F8BB10;
  height: 10px;
  width: 10px;
  position: absolute;
  left: -31px;
  top: 3px;
  box-shadow: 0 0 0 6px #fff;
  border-radius: 50%;
`;

const TimeLineHeader = styled.div`
   > div {
      margin-bottom:.4rem;
   }
`;

const MutedText = styled.div`
 opacity: 0.6;
 font-size: 0.7rem;
`;

const Timeline = ({timeframe, jobTitle, company, location, description}) => (
    <TimeLineItem className="timeline__item">
      <TimeLinePoint className="timeline__point"/>
      <TimeLineHeader>
        <MutedText>{timeframe}</MutedText>
        <h3>{jobTitle}</h3>
        <h6>{company}</h6>
        <MutedText>{location}</MutedText>
      </TimeLineHeader>
      <div className="timeline__content"
           dangerouslySetInnerHTML={{ __html: marked(description) }}>
      </div>
    </TimeLineItem>
);

export default Timeline;
