import React from "react";
import avatar from "../assets/me2.jpg";

import styled from 'styled-components';
import {viewports} from "../utils/viewports";

const Wrapper = styled.div`
  padding-top: 30px;
  padding-bottom: 30px;
  
  @media ${viewports.large} {
    padding-bottom: 0;
    padding-top: 0;
  }
`;

const Image = styled.img`
  border-radius: 50%;
  width: 13rem;
  height: 13rem;
`;

const Avatar = () => (
    <Wrapper>
      <Image src={avatar} alt="Luis Eduardo"/>
    </Wrapper>
);

export default Avatar;
