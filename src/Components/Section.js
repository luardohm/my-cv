import React from "react";
import styled from "styled-components";


import SectionTitle from "../Components/SectionTitle";

const SectionWrapper = styled.div`
  position: relative;
`;

const BackgroundLeft = styled.div`
  position: relative;
`;

const BackgroundRight = styled.div`
  position: relative;
`;

const Section = ({ title, subtitle, body, image, modifierClass }) => {
  const header = image || <SectionTitle title={title} subtitle={subtitle} />;
  const classNames = [
    "section",
    modifierClass ? "section--" + modifierClass : null
  ];

  return (
    <div className={classNames.join(" ")}>
      <div className="section__background section__background--left" />
      <div className="section__background section__background--right" />
      <div className="section__inner">
        <div className="section__header">{header}</div>
        <div className="section__content">{body}</div>
      </div>
    </div>
  );
};

export default Section;
