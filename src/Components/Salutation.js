import React from "react";

const Salutation = ({ text }) => <div className="salutation">{text}</div>;

export default Salutation;
