import React from "react";
import styled from "styled-components";

import {viewports} from '../utils/viewports';

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  padding-bottom: 25px;
  text-align: left;
  
  @media ${viewports.large} {
   flex-grow: 2;
  }
`;

const Count = styled.div`
  width: 100px;
  color: #F8BB10;
  font-weight: bold;
  line-height: 1.5;
`;

const ListItem = () => (
    <Wrapper>
      <Count>01</Count>
      <div>
        <h4>ADVANCED CSS</h4>
        <p>
          Cras ornare tristique elit lorem ipsum dolor sit amet, consectetuer
          adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec
          urna. In nisi neque, aliquet vel, dapibus id.
        </p>
      </div>
    </Wrapper>
);

export default ListItem;
