import React from "react";
import { BrowserRouter as Router, Link } from "react-router-dom";

const Navigation = () => (
  <ul className="navigation">
    <li className="navigation__item">
      <Link to="/about">About me</Link>
    </li>
    <li className="navigation__item">
      <Link to="/">Expertise</Link>
    </li>
    <li className="navigation__item">
      <Link to="/">Skills</Link>
    </li>
    <li className="navigation__item">
      <Link to="/">Experience</Link>
    </li>
    <li className="navigation__item">
      <Link to="/">Contact</Link>
    </li>
  </ul>
);

export default Navigation;
