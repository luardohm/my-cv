import React from "react";
import styled from "styled-components";

import ProgressBar from "../Components/ProgressBar";

const SkillsWrapper = styled.div`
 width: 100%;
 margin: 25px;
`;

const SkillsList = styled.div`
 width: 100%;
`;

const Skills = () => (
  <SkillsWrapper>
    <SkillsList>
      <ProgressBar percent="100" title="Angular 2+" />
      <ProgressBar percent="100" title="Typescript" />
      <ProgressBar percent="70" title="React JS" />
      <ProgressBar percent="70" title="Vue JS" />
      <ProgressBar percent="100" title="HTML/CSS" />
      <ProgressBar percent="90" title="PHP" />
      <ProgressBar percent="100" title="Wordpress" />
      <ProgressBar percent="90" title="Magento" />
    </SkillsList>
  </SkillsWrapper>
);

export default Skills;
