import React from "react";
import styled from "styled-components";

import Title from "../Components/Title";
import ContactItem from "../Components/ContactItem";
import {viewports} from "../utils/viewports";

const ContactInfo = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  padding-top: 55px;
  padding-bottom: 55px;
  border-top: 1px solid #cccccc;
  
  @media ${viewports.large} {
    padding: 25px 0 0 0;
    position: absolute;
    bottom: 25px;
    width: 70%;
    left: 25px;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
  }
`;
const Intro = () => (
    <main>
      <Title name="Luis Eduardo" lastname="R. Gamero" position="Senior Frontend Engineer"/>
      <ContactInfo>
        <ContactItem label="Address" text="Dortmunder Str. 6 10555"/>
        <ContactItem label="Telephone" text="16094 68 4993"/>
        <ContactItem label="E-mail" text="luardohm@gmail.com"/>
      </ContactInfo>
    </main>
);

export default Intro;
