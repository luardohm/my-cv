import React, { Component } from "react";

import {
  withStyles,
  MuiThemeProvider,
  createMuiTheme
} from "@material-ui/core/styles";
import yellow from "@material-ui/core/colors/yellow";

import classNames from "classnames";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

const styles = theme => ({
  palette: {
    primary: yellow
  },
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200
  },
  dense: {
    marginTop: 19
  },
  menu: {
    width: 400
  },
  button: {
    margin: theme.spacing.unit,
    color: "#ffffff"
  },
  cssRoot: {
    color: "#ffffff"
  }
});

const theme = createMuiTheme({
  palette: {
    primary: yellow
  },
  typography: { useNextVariants: true }
});

class ContactMe extends Component {
  state = {
    name: "",
    company: "",
    description: "",
    currency: "EUR"
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  render() {
    const { classes } = this.props;

    return (
      <div className="contact-me">
        <h2>Got some cool project?</h2>
        <MuiThemeProvider theme={theme}>
          <TextField
            id="standard-name"
            label="Name"
            className={classes.textField}
            value={this.state.name}
            onChange={this.handleChange("name")}
            margin="normal"
          />

          <TextField
            id="standard-name"
            className={classes.textField}
            label="Company"
            value={this.state.company}
            onChange={this.handleChange("company")}
            margin="normal"
          />

          <TextField
            id="standard-name"
            label="Email"
            className={classes.textField}
            value={this.state.email}
            onChange={this.handleChange("email")}
            margin="normal"
          />

          <TextField
            id="standard-name"
            style={{ margin: 8 }}
            label="Description"
            multiline={true}
            fullWidth={true}
            rows="4"
            helperText="Provide budget if available"
            placeholder="Tell me some details, tech stack, company size, industry, value proposition..."
            onChange={this.handleChange("description")}
            value={this.state.description}
            margin="normal"
          />

          <Button
            size="large"
            variant="contained"
            color="primary"
            className={classNames(classes.margin, classes.cssRoot)}
          >
            Send enquiry
          </Button>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default withStyles(styles)(ContactMe);
