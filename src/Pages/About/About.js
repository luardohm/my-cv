import React, { Component } from "react";
import PropTypes from "prop-types";

import ContentfulService from "../../Services/ContentfulService";
import Paragraph from "../../Components/Paragraph";

export class About extends Component {
  static propTypes = {
    pageId: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    this.pageId = this.props.pageId;
    this.contentful = new ContentfulService();
    this.state = {
      page: []
    };
  }

  componentWillMount() {
    this.contentful
      .getEntry(this.pageId)
      .then(response => {
        this.setState({
          page: response.fields
        });
      })
      .catch(console.error);
  }

  render() {
    const {
      page: { content }
    } = this.state;
    return <Paragraph content={content} />;
  }
}
