import React, { Component } from "react";
import styled from "styled-components";

import Timeline from "../../Components/Timeline";
import ContentfulService from "../../Services/ContentfulService";

const TimeLineWrapper = styled.div`
 border-left-width: 2px;
 border-left-style: solid;
 margin-left: calc(1rem - 2px);
 padding-left: 25px;
 position: relative;
 border-color: #ececec;
 text-align: left;
`;

export class Experience extends Component {
  constructor(props) {
    super(props);
    this.contentful = new ContentfulService();
    this.state = {
      experienceContent: []
    };
  }

  componentWillMount() {
    this.contentful
      .getJobExperiences()
      .then(({ items }) => this.setWorkExperience(items));
  }

  setWorkExperience(items) {
    const content = items.reverse();
    this.setState({ experienceContent: content });
  }

  render() {
    const { experienceContent } = this.state;
    return (
      <TimeLineWrapper>
        {experienceContent.map(({ fields }, i) => {
          return (
            <Timeline
              key={i}
              timeframe={fields.timeframe}
              jobTitle={fields.title}
              company={fields.company}
              location={fields.location}
              description={fields.description}
            />
          );
        })}
      </TimeLineWrapper>
    );
  }
}
